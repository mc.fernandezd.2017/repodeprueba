# Ejercicio 16.2. Subida de programa a repo de GitLab

def factorial(num):
    if num == 0:
        return 1
    else:
        return num * factorial(num - 1)


for i in range(1, 11):
    print(f"Factorial de {i}: {factorial(i)}")
